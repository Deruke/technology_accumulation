package com.mmall.concurrency.example.singleton;

import com.mmall.concurrency.annoations.NotRecommend;
import com.mmall.concurrency.annoations.NotThreadSafe;

/**
 * Created by 千叶星城 on 2018/9/18.
 * 懒汉模式
 * 单例实例在第一次使用时进行创建
 */
/**
 * 单线程模式下没有问题
 * 多线程模式在静态方法中出现问题
 * 当多个线程访问这个方法时，可能会创建多个对象出来
 * 然后执行多个方法
 *
 */

/**
 * 懒汉模式线程安全模式
 * 问题：
 * 同一个时间内只允许一个线程访问这个方法保证线程安全
 *造成性能开销
 *
 */
@NotThreadSafe
@NotRecommend
public class SingletonExample3 {

    // 私有构造函数
    private SingletonExample3(){

    }

    // 单例对象
    private static SingletonExample3 instance = null;

    // 静态的工厂方法
    public static synchronized SingletonExample3 getInstance(){
        if (instance == null){
            instance = new SingletonExample3();
        }
        return instance;
    }
}
