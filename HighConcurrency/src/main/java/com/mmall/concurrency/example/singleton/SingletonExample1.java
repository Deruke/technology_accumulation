package com.mmall.concurrency.example.singleton;

/**
 * Created by 千叶星城 on 2018/9/18.
 * 懒汉模式
 * 单例实例在第一次使用时进行创建
 */

import com.mmall.concurrency.annoations.NotThreadSafe;

/**
 * 单线程模式下没有问题
 * 多线程模式在静态方法中出现问题
 * 当多个线程访问这个方法时，可能会创建多个对象出来
 * 然后执行多个方法
 *
 */
@NotThreadSafe
public class SingletonExample1 {

    // 私有构造函数
    private SingletonExample1(){

    }

    // 单例对象
    private static SingletonExample1 instance = null;

    // 静态的工厂方法
    public static SingletonExample1 getInstance(){
        if (instance == null){
            instance = new SingletonExample1();
        }
        return instance;
    }
}
