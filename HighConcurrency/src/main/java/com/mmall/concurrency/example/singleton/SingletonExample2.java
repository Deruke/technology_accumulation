package com.mmall.concurrency.example.singleton;



import com.mmall.concurrency.annoations.ThreadSafe;
/**
 * Created by 千叶星城 on 2018/9/18.
 * 饿汉模式
 * 单例实例在类装载时进行创建
 */

/**
 * 处理太多
 * 造成加载慢，性能问题
 *只进行类的加载，不进行实际调用造成资源的浪费
 */
@ThreadSafe
public class SingletonExample2 {

    // 私有构造函数
    private SingletonExample2(){

    }

    // 单例对象
    private static SingletonExample2 instance = new SingletonExample2();

    // 静态的工厂方法
    public static SingletonExample2 getInstance(){
        return instance;
    }
}
