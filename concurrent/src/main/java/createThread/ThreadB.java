package createThread;

/**
 * Created by 千叶星城 on 2018/8/27.
 */
public class ThreadB implements Runnable{
    @Override
    public void run() {
        try {
            Thread.sleep(500L);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("这是线程B：");
    }

    public static void main(String[] args) {
        ThreadB threadB = new ThreadB();
        new Thread(threadB).start();
        System.out.println("这是主线程：");
    }
}
