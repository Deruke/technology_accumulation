package createThread;

/**
 * Created by 千叶星城 on 2018/8/27.
 */
public class ThreadBInfo implements Runnable{
    @Override
    public void run() {
        try {
            Thread.sleep(500L);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("这是线程B：");
        Thread curThread = Thread.currentThread();
        String curThreadName = curThread.getName();
        System.out.println("线程名称："+ curThreadName);
        System.out.println("返回该线程组数目："+Thread.activeCount());
        System.out.println("标识"+curThread.getId());
        System.out.println("激活状态："+curThread.isAlive());
        System.out.println("守护线程："+curThread.isDaemon());
    }

    public static void main(String[] args) {
        ThreadBInfo threadB = new ThreadBInfo();
        //new Thread(threadB).start();
        System.out.println("这是主线程：");
        for (int i=0;i<5;i++){
            new Thread(threadB,"线程名称：("+i+")").start();
        }
        Thread threadMain = Thread.currentThread();
        System.out.println("线程数目："+Thread.activeCount());

    }
}
