package createThread;

/**
 * Created by 千叶星城 on 2018/8/27.
 * 创建线程
 * 继承extends Thread 覆盖run（）方法
 */
public class ThreadA extends Thread{
    public void run(){
        super.run();
        try {
            Thread.sleep(500L); // 500ms
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("这是线程A");
    }

    public static void main(String[] args) {
        ThreadA threadA = new ThreadA();
        threadA.start();
        System.out.println("这是主线程：");
    }
}
