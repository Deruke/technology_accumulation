# AbstractQueuedSynchronizer -AQS
![11](img/11.png)

* 子类通过继承并通过实现它的方法管理其状态(acquire和release)的方法操作状态
* 可以同时实现排它锁和共享锁模式(独占、共享)

* 1、CountDownLatch
* 2、Semaphore
* 3、ReentrantLock
* 4、Condition
* 5、FutureTask


## ReentrantLock与锁
### ReentrantLock（可重入锁）（优）和 synchronized的区别
* 可重入性
* 锁的实现
* 性能的区别
* 功能区别

# ReentrantLock独有的功能
* 可指定是公平锁还是非公平锁
* 提供了一个Condition类，可以分组唤醒需要唤醒的线程
* 提供能够中断等待锁的线程的机制，lock.lockInterruptibly()

# J.U.C
## FutureTask
* Callable与Runnable接口对比
* Future接口
* FutureTask类

# BlockingQueue
* ArrayBlockingQueue
* DelayQueue
* LinkedBlockingQueue
* PriorityBlockingQueue
* SynchronousQueue






