package demo.controller;

import com.example.po.Book;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 千叶星城 on 2018/9/6.
 */
@RestController
public class HelloWorldController {

    @RequestMapping(value = "/hello")
    public String helloActuator() {
        Book book = Book.builder().Id(1111L).reader("hj").build();
        System.out.println(book.toString());
        return book.toString();
    }
}
