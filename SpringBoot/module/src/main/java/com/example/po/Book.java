package com.example.po;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * Created by 千叶星城 on 2018/9/6.
 */
@Data
@Entity
@ToString
@Builder
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;
    private String reader;
    private String isbn;
    private String title;
    private String author;
    private String description;


}
