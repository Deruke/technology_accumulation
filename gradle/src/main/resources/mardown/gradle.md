# gradle构建项目
## 基本原理
* 构建脚本介绍
* 依赖管理
## 深入实战
* 多项目构建
* 测试
* 发布

![image](img/image.png)
## 项目演示
* TODO应用程序，只实现添加待办事项
* java应用程序
* Web版
### 构建脚本
![image_1](img/image_1.png)
![image_2](img/image_2.png)
### 项目(project)
* 坐标 (group、name、version)
* 方法 (apply、dependencies、repositories、task)
![image_3](img/image_3.png)
* dependsOn doFirst doLast

* 初始化 配置 执行
* 常用仓库：mavenLocal、mavenCentral、jcenter、自定义仓库
![image_4](img/image_4.png)
## 依赖
* run compile
* testRun testCompile
添加依赖方式
```gradle
dependencies {
    compile 'ch.qos.logback:logback-classic:1.2.1'
    testCompile group: 'junit', name: 'junit', version: '4.11'
}
```
## 解决版本冲突
![image_5](img/image_5.png)

## 多项目构建
![image_6](img/image_6.png)
![image_7](img/image_7.png)
### 配置子项目
* 所有项目添加logback日志功能
* 统一配置公共属性
## 添加项目模块
![image_8](img/image_8.png)
```gradle
dependencies {
    comile project(":model")
    compile 'ch.qos.logback:logback-classic:1.2.1'
    testCompile group: 'junit', name: 'junit', version: '4.11'
}
```
## 自动化测试
![image_9](img/image_9.png)
![image_10](img/image_10.png)

## 发布
![image_11](img/image_11.png)