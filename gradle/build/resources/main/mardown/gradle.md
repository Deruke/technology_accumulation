# gradle构建项目
## 基本原理
* 构建脚本介绍
* 依赖管理
## 深入实战
* 多项目构建
* 测试
* 发布

![image](img/image.png)
## 项目演示
* TODO应用程序，只实现添加待办事项
* java应用程序
* Web版
### 构建脚本
![image_1](img/image_1.png)
![image_2](img/image_2.png)
### 项目(project)
* 坐标 (group、name、version)
* 方法 (apply、dependencies、repositories、task)
![image_3](img/image_3.png)
* dependsOn doFirst doLast
