package com.mmall.concurrency.example.singleton;



import com.mmall.concurrency.annoations.ThreadSafe;
/**
 * Created by 千叶星城 on 2018/9/18.
 * 饿汉模式
 * 单例实例在类装载时进行创建
 */

/**
 * 处理太多
 * 造成加载慢，性能问题
 *只进行类的加载，不进行实际调用造成资源的浪费
 */
@ThreadSafe
public class SingletonExample6 {

    // 静态方法按顺序执行

    // 单例对象
    private static SingletonExample6 instance = null;

    static {
        instance = new SingletonExample6();
    }

    // 静态的工厂方法
    public static SingletonExample6 getInstance() {
        return instance;
    }

    public static void main(String[] args) {
        System.out.println(getInstance().hashCode());
        System.out.println(getInstance().hashCode());
    }
}
