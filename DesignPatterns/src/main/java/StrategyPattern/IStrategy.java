package StrategyPattern;

/**
 * Created by 千叶星城 on 2018/8/27.
 */
public interface IStrategy {
    // 可执行算法
    public void operate();

}
