package StrategyPattern;

/**
 * Created by 千叶星城 on 2018/8/27.
 */
public class BlockEnemy implements IStrategy{
    @Override
    public void operate() {
        System.out.println("孙夫人断后，挡住追兵");
    }
}
