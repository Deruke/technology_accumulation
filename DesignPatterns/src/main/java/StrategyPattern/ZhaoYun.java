package StrategyPattern;

/**
 * Created by 千叶星城 on 2018/8/27.
 */
public class ZhaoYun {

    // 赵云依次拆开妙计
    public static void main(String[] args) {
        Context context;

        // 拆开第一个
        System.out.println("第一个");
        context = new Context(new BackDoor());// 拿到妙计
        context.operate();// 拆开持行
        System.out.println("--------------------------");

        // 第二个
        System.out.println("拆开第二个");
        context = new Context(new GivenGreenLight());
        context.operate();
        System.out.println("----------------------------");

        // 第三个
        System.out.println("拆开第三个");
        context = new Context(new BlockEnemy());
        context.operate();
        System.out.println("----------------------------------");
    }
}
