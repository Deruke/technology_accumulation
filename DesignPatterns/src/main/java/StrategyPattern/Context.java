package StrategyPattern;

/**
 * Created by 千叶星城 on 2018/8/27.
 */
public class Context {

    private IStrategy strategy;
    public Context(IStrategy strategy){
        this.strategy = strategy;
    }

    // 使用计谋
    public void operate(){
        this.strategy.operate();
    }
}
