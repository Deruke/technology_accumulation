package StrategyPattern;

/**
 * Created by 千叶星城 on 2018/8/27.
 */
public class GivenGreenLight implements IStrategy{
    @Override
    public void operate() {
        System.out.println("求吴国太开个绿灯，放行");
    }
}
