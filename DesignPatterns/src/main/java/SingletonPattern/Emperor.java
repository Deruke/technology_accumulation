package SingletonPattern;

/**
 * Created by 千叶星城 on 2018/9/5.
 */
public class Emperor {

    private static final Emperor emperor = new Emperor();//初始化一个皇帝

    private Emperor(){
        // 定义私有的类，外部类无法new，只有在这个类创建，通过
        // getInstance()方法访问对象
    }

    public static Emperor getInstance(){
        return emperor;
    }
    // 类中方法尽量使用静态
    public static void say(){
        System.out.println("我就是皇帝");
    }
}
