package SingletonPattern;

/**
 * Created by 千叶星城 on 2018/9/5.
 */
public class Minister {

    public static void main(String[] args) {
       for (int day = 0;day < 3;day++){
           Emperor emperor = Emperor.getInstance();
           emperor.say(); // 都是同一个皇帝
       }
    }
}
